# ip

#### 介绍
省市区

#### 安装教程
```shell script
php composer.phar require cuifox/region:*
```

#### 使用说明
```php
use CuiFox\Region\Region;

// 所有省份
$provinces = Region::provinces();
var_dump($provinces);

// 省份所有市
$cities = Region::cities(140402);
var_dump($cities);

// 市所有区县
$districts = Region::districts(140411);
var_dump($districts);

// 全名
$fullName = Region::fullName(130402);
var_dump($fullName);
```